<?php

require_once('animals.php');
require_once('frog.php');
require_once('ape.php');

$object = new animals("shaun");

echo "Name : " . $object -> sheep . "<br>";
echo "legs : " . $object -> legs . "<br>";
echo "cold blooded : " . $object -> cold_blooded . "<br><br>";

$object2 = new frog("buduk");

echo "Name : " . $object2 -> kodok . "<br>";
echo "legs : " . $object2 -> legs . "<br>";
echo "cold blooded : " . $object2 -> cold_blooded . "<br>";
echo $object2 -> jump() . "<br><br>";

$object3 = new ape("kera sakti");

echo "Name : " . $object3 -> sungokong . "<br>";
echo "legs : " . $object3 -> legs . "<br>";
echo "cold blooded : " . $object3 -> cold_blooded . "<br>";
echo $object3 -> yell() . "<br><br>";

?>