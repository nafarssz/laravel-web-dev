SOAL 1

Soal 1 : membuat database
jawaban 1 :
maaf kang yang nomor satu gak bisa di copy karna aku close cmd nya tapi databse nya udh aku buat!

_________________________________________________
SOAL 2 

Soal 2 : membuat table

MariaDB [myshop]> create table users(
    -> id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    -> name VARCHAR(255),
    -> email VARCHAR(255),
    -> password VARCHAR(255)
    -> );
Query OK, 0 rows affected (0.365 sec)
MariaDB [myshop]> DESCRIBE users;
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| id       | int(11)      | NO   | PRI | NULL    | auto_increment |
| name     | varchar(255) | YES  |     | NULL    |                |
| email    | varchar(255) | YES  |     | NULL    |                |
| password | varchar(255) | YES  |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+
4 rows in set (0.112 sec)

MariaDB [myshop]> CREATE TABLE categories(
    -> id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    -> name VARCHAR(255)
    -> );
Query OK, 0 rows affected (0.247 sec)
MariaDB [myshop]> DESCRIBE categories;
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | int(11)      | NO   | PRI | NULL    | auto_increment |
| name  | varchar(255) | YES  |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+
2 rows in set (0.008 sec)

MariaDB [myshop]> CREATE TABLE items(
    -> id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    -> name VARCHAR(255),
    -> description VARCHAR(255),
    -> price int,
    -> stock int,
    -> category_id int,
    -> FOREIGN KEY (category_id) REFERENCES categories (id)
    -> );
Query OK, 0 rows affected (0.252 sec)
MariaDB [myshop]> DESCRIBE items;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| id          | int(11)      | NO   | PRI | NULL    | auto_increment |
| name        | varchar(255) | YES  |     | NULL    |                |
| description | varchar(255) | YES  |     | NULL    |                |
| price       | int(11)      | YES  |     | NULL    |                |
| stock       | int(11)      | YES  |     | NULL    |                |
| category_id | int(11)      | YES  | MUL | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+
6 rows in set (0.009 sec)
__________________________________________________________________
SOAL 3

Soal 3 : menambahkan data ke dalam table

MariaDB [myshop]> INSERT INTO users (name, email, password)
    -> VALUES ("Jane Doe", "jane@doe.com", "janita123");
Query OK, 1 row affected (0.119 sec)
MariaDB [myshop]> SELECT * FROM users;
+----+----------+--------------+-----------+
| id | name     | email        | password  |
+----+----------+--------------+-----------+
|  1 | John Doe | john@doe.com | john123   |
|  2 | Jane Doe | jane@doe.com | janita123 |
+----+----------+--------------+-----------+
2 rows in set (0.001 sec)

MariaDB [myshop]> INSERT INTO categories (name)
    -> VALUES ("gadget"),
    -> ("cloth"),
    -> ("men"),
    -> ("women"),
    -> ("branded");
Query OK, 5 rows affected (0.057 sec)
Records: 5  Duplicates: 0  Warnings: 0
MariaDB [myshop]> SELECT * FROM categories;
+----+---------+
| id | name    |
+----+---------+
|  1 | gadget  |
|  2 | cloth   |
|  3 | men     |
|  4 | women   |
|  5 | branded |
+----+---------+
5 rows in set (0.001 sec)

MariaDB [myshop]> INSERT INTO items (name, description, price, stock, category_id)
    -> VALUES ("sumsang b50", "hape keren dari merek sumsang", "4000000", "100", "1"),
    -> ("uniklooh", "baju keren dari brand ternama", "500000", "50", "2"),
    -> ("IMHO watch", "jam tangan anak yang jujur banget", "2000000", "10", "1");
Query OK, 3 rows affected (0.055 sec)
Records: 3  Duplicates: 0  Warnings: 0
MariaDB [myshop]> SELECT * INTO items;
ERROR 1327 (42000): Undeclared variable: items
MariaDB [myshop]> SELECT * FROM items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  2 | sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  3 | uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  4 | IMHO watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.001 sec)
____________________________________________________________________
SOAL 4

Soal 4 : mengambil data dari data base

Soal 4 (A) : mengambil data dari users
MariaDB [myshop]> SELECT id, name, email from users;
+----+----------+--------------+
| id | name     | email        |
+----+----------+--------------+
|  1 | John Doe | john@doe.com |
|  2 | Jane Doe | jane@doe.com |
+----+----------+--------------+
2 rows in set (0.001 sec)

Soal 4 (B) : mengambil data dari items
MariaDB [myshop]> SELECT * FROM items where price >=1000000;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  2 | sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  4 | IMHO watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
2 rows in set (0.010 sec)
MariaDB [myshop]> SELECT * FROM items WHERE name like '%watch%';
+----+------------+-----------------------------------+---------+-------+-------------+
| id | name       | description                       | price   | stock | category_id |
+----+------------+-----------------------------------+---------+-------+-------------+
|  4 | IMHO watch | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+------------+-----------------------------------+---------+-------+-------------+
1 row in set (0.001 sec)

Soal 4 (C) : Menampilkan data items join dengan kategori
MariaDB [myshop]> SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori
    -> FROM items
    -> left join categories
    -> on items.category_id = categories.id;
+-------------+-----------------------------------+---------+-------+-------------+----------+
| name        | description                       | price   | stock | category_id | kategori |
+-------------+-----------------------------------+---------+-------+-------------+----------+
| sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 | gadget   |
| uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 | cloth    |
| IMHO watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget   |
+-------------+-----------------------------------+---------+-------+-------------+----------+
3 rows in set (0.006 sec)
________________________________________________________________________________________________
SOAL 5

Soal 5 : Soal 5 Mengubah Data dari Database
MariaDB [myshop]> update items set price = 2500000 where name = 'sumsang b50';
Query OK, 1 row affected (0.050 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop]> SELECT * FROM items
    -> ;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  2 | sumsang b50 | hape keren dari merek sumsang     | 2500000 |   100 |           1 |
|  3 | uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  4 | IMHO watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.001 sec)




