<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);

Route::get('/register', [HomeController::class, 'bio']);

Route::post('/isidata', [AuthController::class, 'isidata']);

Route::get('/master', function(){
    return view('master');
});

Route::get('/table', function(){
    return view('table.table');
});

Route::get('/datatable', function(){
    return view('table.datatable');
});